import { gql } from "@apollo/client";

export const GET_APPOINTMENT_BY_CLIENT = gql`
  query {
    appointmentByClient (clientContactReferenceID:118565) {
      id
      dateEnd
      dateBegin
      dateAdded
    }
  }
`;
